export default class Renderer extends PIXI.Container {
  constructor(app, rows, cols, rowOffset, blockSize) {
    super();
    this.app = app;
    this.rows = rows;
    this.cols = cols;
    this.rowsOffset = rowOffset;
    this.blockSize = blockSize;
    this.textures = this.app.loader.resources.data.textures;

    this.sprites = [];

    for (let i = 0; i < this.rows; ++i) {
      let row = [];
      for (let j = 0; j < this.cols; ++j) {
        let spr = new PIXI.Sprite(this.textures.background);
        row.push(spr);
        console.log(row);
        spr.x = j * blockSize;
        spr.y = i * blockSize;
        spr.blockColor = null;
        this.addChild(spr);
      }
      this.sprites.push(row);
    }
  }

  updateColor(row, col, color) {
    if (row < 0) return;
    let sprite = this.sprites[row][col];
    if (sprite.blockColor != color) {
      sprite.blockColor = color;
      sprite.texture = this.textures[color] || this.textures.background;
    }
  }

  updateFromBoard(board) {
    for (let i = 0; i < this.rows; ++i) {
      for (let j = 0; j < this.cols; ++j) {
        this.updateColor(i, j, board.get(i + this.rowsOffset, j));
      }
    }
  }

  updateFromTetromino(tetromino) {
    if (tetromino) {
      tetromino.absolutePos().forEach((pos) => {
        this.updateColor(pos[0] - this.rowsOffset, pos[1], tetromino.color);
      });
    }
  }
}
