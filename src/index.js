import Game from "./Game";
import config from './config/config';

let app = new PIXI.Application({
  width: config.display.width,
  height: config.display.height
});

document.body.appendChild(app.view);
document.body.style.display = 'flex';
document.body.style.justifyContent = 'center';
document.body.style.alignItems = 'center';
document.body.style.background = '#1E2C52';

// let hintBlock = new PIXI.Graphics();
// hintBlock.beginFill();
// hintBlock.drawRect(config.display.width, 0, 200, 200);
// hintBlock.endFill();
// app.addChild(hintBlock);

let game = new Game(app);
app.loader
  .add("data", "../assets/images/sprites.json")
  .load(() => game.run());

