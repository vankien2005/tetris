export default class State extends PIXI.Container {
  constructor() {
    super();
    this.visible = false;
  }

  enter(opts) {}

  exit(opts) {}

  update(dt) {}
}
