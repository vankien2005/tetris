import Tetromino, { SHAPE_COLORS } from "./Tetromino";

const shapeTypes = Object.keys(SHAPE_COLORS).join("");
export default class TetronimoSpawner {
  constructor() {
    this.queue = [];
    this.refillQueue();

    console.log(shapeTypes);
  }

  refillQueue() {
    let a = (shapeTypes + shapeTypes + shapeTypes).split('');
   
    for (let i = a.length; i > 0; --i) {
       console.log(a);
      let j = Math.floor(Math.random() * i);
      let tmp = a[i - 1];
      a[i - 1] = a[j];
      a[j] = tmp;
      console.log(a[j]);
    }
    this.queue = a.concat(this.queue);
  }

  spawn() {
    if (this.queue.length < 2) {
      this.refillQueue();
    }
    return new Tetromino(this.queue.pop());
  }
}
