import MenuGame from "./MenuGame";
export default class GameStart extends MenuGame {
  constructor(game) {
    super(game, "TETRIS");
  }

  update(dt) {
    super.update(dt);

    if (this.game.key.space.trigger()) {
      this.game.setState("play", { restart: true });
    }
  }
}
