import MenuGame from "./MenuGame";

export default class GamePause extends MenuGame {
  constructor(game) {
    super(game, 'GAME PAUSE', 'Press SPACE to continue');

    this.scoreInfo = new PIXI.Text('Last score', this.info.style);
    this.scoreInfo.anchor.set(0.5);
    this.scoreInfo.x = this.game.app.view.width * 0.5;
    this.scoreInfo.y = this.game.app.renderer.height * 0.5;
    this.addChild(this.scoreInfo);
  }

  enter(opts) {
    if (opts.score) {
      this.scoreInfo.text = `Score: ${opts.score.points}\nLines: ${opts.score.lines}`;
      this.scoreInfo.visible = true;
    } else {
      this.scoreInfo.visible = false;
    }
  }

  update(dt) {
    super.update(dt);
    if (this.game.key.space.trigger()) {
      this.game.setState('play', { restart: false });
    }
  }
}