import State from "../State";

export const simpleTextStyle = new PIXI.TextStyle({
  fontFamily: "Arial",
  fontSize: 18,
  fill: "#FFF1E9",
  stroke: "#000000",
  strokeThickness: 4,
});

export const fancyTextStyle = new PIXI.TextStyle({
  fontFamily: "Arial",
  fontSize: 42,
  fontWeight: "bold",
  fill: ["#FD79A8", "#34AEFC", "#FC1051"],
  stroke: "#000000",
  strokeThickness: 4,
});


export default class MenuGame extends State {
  constructor(game, titleText = "TETRIS", infoText = "Press SPACE to play") {
    super();

    this.game = game;

    this.background = new PIXI.Graphics();
    this.background.beginFill();
    this.background.drawRect(
      0,
      0,
      this.game.app.renderer.width,
      this.game.app.renderer.height
    );
    this.background.endFill();
    this.addChild(this.background);

    this.title = new PIXI.Text(titleText, fancyTextStyle);
    this.title.anchor.set(0.5);
    this.title.x = this.game.app.view.width * 0.5;
    this.title.y = this.game.app.renderer.height * 0.2;
    this.addChild(this.title);

    this.info = new PIXI.Text(infoText, simpleTextStyle);
    this.info.anchor.set(0.5);
    this.info.x = this.game.app.view.width * 0.5;
    this.info.y = this.game.app.renderer.height * 0.9;
    this.addChild(this.info);
  }
}
