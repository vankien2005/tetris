import MenuGame from './MenuGame';
export default class GameOver extends MenuGame {
  constructor(game) {
    super(game, 'GAMEOVER');
    this.scoreInfo = new PIXI.Text('Score', this.info.style);
    this.scoreInfo.anchor.set(0.5);
    this.scoreInfo.x = this.game.app.view.width * 0.5;
    this.scoreInfo.y = this.game.app.renderer.height * 0.5;
    this.addChild(this.scoreInfo);
  }
  enter(opts) {
        let score = this.game.scores.get();
        this.scoreInfo.text = `Score: ${score.points}\nLines: ${score.lines}`;
    }
  update(dt) {
    super.update(dt);

    if (this.game.key.space.trigger()) {
      this.game.setState("play", { restart: true });
    }
  }
}

