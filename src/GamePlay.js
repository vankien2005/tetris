import Renderer from "./Renderer";
import config from "./config/config";
import State from "./State";
import Board from "./Board";
import TetronimoSpawner from "./TetronimoSpawner";
export default class GamePlay extends State {
  constructor(game, app) {
    super();
    this.app = app;
    this.game = game;

    this.board = null;
    this.spawner = null;
    this.tetromino = null;
    this.renderer = new Renderer(
      this.app,
      config.game.rows,
      config.game.cols,
      config.game.hiddenRows,
      config.display.blockSize
    );
    this.addChild(this.renderer);
  }

  enter(opts) {
    if (opts.restart || this.board == null) {
      this.board = new Board(
        config.game.rows + config.game.hiddenRows,
        config.game.cols
      );
      this.spawner = new TetronimoSpawner();

      this.tetromino = null;
      this.tetrominoFallSpeed = config.game.fallSpeed;
      this.tetrominoFallSpeedMin = config.game.fallSpeedMin;
      this.tetrominoDropModifier = config.game.dropModifier;

      this.tetrominoFallTimer = this.tetrominoFallSpeed;

      this.rowsCleared = 0;
      this.score = 0;

      this.spawnTetromino();
    }
  }

  update(dt) {
    if (this.game.key.space.trigger()) {
      this.game.setState("gamepause", {
        keepVisible: true,
        score: {
          points: this.score,
          lines: this.rowsCleared,
        },
      });
    }

    if (this.tetromino) {
      this.updateTetromino();
    }

    this.renderer.updateFromBoard(this.board);
    // console.log(this.board);
    // console.log(this.tetromino);
    this.renderer.updateFromTetromino(this.tetromino);
  }

  spawnTetromino() {
    this.tetromino = this.spawner.spawn();
    this.tetromino.row = 0;
    this.tetromino.col = this.board.cols / 2 - 2;

    console.log(this.tetromino);
    if (this.board.collides(this.tetromino.absolutePos(0, 0))) {
      this.lockTetromino();
      this.gameOver();
    }
  }

  lockTetromino() {
    let fullRows = this.board.setAll(
      this.tetromino.absolutePos(),
      this.tetromino.color
    );
    this.tetromino = null;

    if (fullRows.length > 0) {
      this.updateScore(fullRows.length);
      this.board.cleanRows(fullRows);
    }
  }

  gameOver() {
    this.game.scores.add(this.rowsCleared, this.score);
    this.game.setState("gameover", { keepVisible: true });
  }

  updateTetromino() {
    if (this.game.key.up.trigger()) {
      if (!this.board.collides(this.tetromino.absolutePos(0, 0, true))) {
        this.tetromino.rotate();
      } else if (
        !this.board.collides(this.tetromino.absolutePos(0, -1, true))
      ) {
        --this.tetromino.col;
        this.tetromino.rotate();
      } else if (!this.board.collides(this.tetromino.absolutePos(0, 1, true))) {
        ++this.tetromino.col;
        this.tetromino.rotate();
      }
    }

    if (
      this.game.key.left.trigger() &&
      !this.board.collides(this.tetromino.absolutePos(0, -1))
    ) {
      --this.tetromino.col;
    }
    if (
      this.game.key.right.trigger() &&
      !this.board.collides(this.tetromino.absolutePos(0, 1))
    ) {
      ++this.tetromino.col;
    }

    let tickMod = this.game.key.down.pressed ? this.tetrominoDropModifier : 1;
    if ((this.tetrominoFallTimer -= tickMod) <= 0) {
      if (this.board.collides(this.tetromino.absolutePos(1, 0))) {
        this.lockTetromino();
        this.spawnTetromino();
      } else {
        ++this.tetromino.row;
        this.tetrominoFallTimer = this.tetrominoFallSpeed;
      }
    }
  }

  updateScore(rows) {
    this.rowsCleared += rows;
    this.score += Math.pow(2, rows - 1);
  }
}
