import GamePlay from "./GamePlay";
import Keyboard from "./Keyboard";
import GameOver from "./menu/GameOver";
import GamePause from "./menu/GamePause";
import GameStart from "./menu/GameStart";
import Score from "./score";
export default class Game {
  constructor(app) {
    this.app = app;
    this.gameStates = {};
    this.state = null;
  }
  run() {
    this.key = new Keyboard();
    this.scores = new Score();

    this.addState("play", new GamePlay(this, this.app));
    this.addState("gamestart", new GameStart(this));
    this.addState("gamepause", new GamePause(this));
    this.addState("gameover", new GameOver(this));

    // console.log(this.gameStates);
    this.setState("gamestart", { restart: true });
    this.app.ticker.add(this.update, this);
  }

  addState(stateName, state) {
    this.gameStates[stateName] = state;
    this.app.stage.addChild(state);
    console.log(state);
  }
  update(dt) {
    if (this.state) {
      this.state.update(dt);
    }
  }
  setState(stateName, opts) {
    let oldState = this.state;

    this.state = null;

    if (oldState) {
      if (!opts.keepVisible) {
        oldState.visible = false;
      }
      oldState.exit(opts);
    }

    let newState = this.gameStates[stateName];
    newState.enter(opts);
    newState.visible = true;
    this.state = newState;
  }
}
