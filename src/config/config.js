export const game = {
  cols: 10,
  rows: 20,
  hiddenRows: 2,
  fallSpeed: 40,
  fallSpeedMin: 3,
  fallSpeedupStep: 10,
  fallSpeedupDelay: 1800,
  dropModifier: 10,
};

const SPRITE_SIZE = 32;

export const display = {
  blockSize: SPRITE_SIZE,
  width: game.cols * SPRITE_SIZE,
  height: game.rows * SPRITE_SIZE,
};

export const controls = {
  repeatDelay: 2,
  initialRepeatDelay: 10,
};

export default { game, display, controls };
