export default class Score {
  constructor() {
    this.scores = [];
  }

  add(lines, points) {
    this.scores.unshift({
      lines,
      points
    });
    console.log("New score: ", this.scores[0]);
  }

  get() {
    if (this.scores.length > 0) {
      return this.scores[0];
    }
  }
}
